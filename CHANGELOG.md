## v0.1

First release, as a CLI. Support for Linux with Chrome and Brave, and Windows with Edge, Chrome, and Brave.

### v0.1.1

Fixed mismatch with Chrome and Brave on Windows.
Addded Vivaldi support for windwos.
Removed unneeded dependency.
Added changelog.

### v0.1.2

Added Opera and Opera GX support for Windows.

## v0.2

Made changelog nicer.
Added Vivaldi and Opera support for Linux.
Added Chrome, Brave, Edge, Opera, Opera GX, and Vivaldi support for macOS.
Added GUI client that runs on Linux (and probably macOS).