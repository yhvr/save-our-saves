const level = require("level");
function getSaves(path, url = "jacorb90.github.io", callback) {
	try {
		const db = level(path);
		const found = [];
		const out = {};
		db.createKeyStream()
			.on("data", data => {
				if (data.startsWith(`_${url}`)) found.push(data);
				// I don't know if that's supposed to work or not.
				// It just does.
			})
			.once("end", async () => {
				for (let i = 0; i < found.length; i++) {
					const id = found[i].substring(url.length + 3);
					out[id] = await db.get(found[i]);
					out[id] = out[id].substring(1);
				}
				callback(out);
				
				db.close();
			});
	} catch (e) {
		console.log(`!!! ERROR !!!
SOS couldn't run.
- Did you get the path right?
- Did you close your browser first?`);
	}
}

// where localStorage is
const PATHS = {
	linux: {
		brave: "/home/{user}/.config/BraveSoftware/Brave-Browser/Default/Local Storage/leveldb/",
		opera: "/home/{user}/.config/opera/Local Storage/leveldb/",
		chrome: "/home/{user}/.config/google-chrome/Default/Local Storage/leveldb/",
		vivaldi: "/home/{user}/.config/vivaldi/Default/Local Storage/leveldb/"
	},
	windows: {
		edge: "C:/Users/{user}/AppData/Local/Microsoft/Edge/User Data/Default/Local Storage/leveldb/",
		brave: "C:/Users/{user}/AppData/Local/BraveSoftware/Brave-Browser/User Data/Default/Local Storage/leveldb/",
		opera: "C:/Users/{user}/AppData/Roaming/Opera Software/Opera GX Stable/Local Storage/leveldb/",
		chrome: "C:/Users/{user}/AppData/Local/Google/Chrome/User Data/Default/Local Storage/leveldb/",
		operagx:
			"C:/Users/{user}/AppData/Roaming/Opera Software/Opera Stable/Local Storage/leveldb/",
		vivaldi:
			"C:/Users/{user}/AppData/Local/Vivaldi/User Data/Default/Local Storage/leveldb/",
	},
	mac: {
		edge: "/Users/{user}/Library/Application Support/Microsoft Edge/Default/Local Storage/leveldb/",
		brave: "/Users/{user}/Library/Application Support/BraveSoftware/Brave-Browser/Default/Local Storage/leveldb/",
		opera: "/Users/{user}/Library/Application Support/com.operasoftware.Opera/Local Storage/leveldb/",
		chrome: "/Users/{user}/Library/Application Support/Google/Chrome/Default/Local Storage/leveldb/",
		operagx: "/Users/{user}/Library/Application Support/com.operasoftware.OperaGX/Local Storage/leveldb/",
		vivaldi: "/Users/{user}/Library/Application Support/Vivaldi/Default/Local Storage/leveldb/",
	}
};

module.exports = {
	PATHS, getSaves
};