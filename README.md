# ***THE GUI DOES NOT WORK ON WINDOWS. IF YOU HAVE A COMPUTER WITH WINDOWS AND WOULD LIKE TO TAKE THE TIME TO MAKE IT WORK, YOU HAVE MY PERMISSION.***

# SOS - Save Our Saves

A Node.JS Script to extract saves from browser games moved to another domain.

## GUI Usage

TODO

## CLI Usage

```sh
node cli.js <os> <browser>

# possible OSes:
# windows (browsers: edge, chrome, brave, vivaldi, opera, operagx)
# mac (browsers: edge, brave, opera, chrome, operagx, vivaldi)
# linux (browsers: chrome, brave, opera, vivaldi)
# example: node . linux chrome
# send output to file (linux+mac only?): node . linux chrome >> output.txt
```

*made with ❤️ by yhvr*