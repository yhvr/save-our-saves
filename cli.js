const { PATHS, getSaves } = require("./saver.js");

getSaves(PATHS[process.argv[2]][process.argv[3]].replace(
	/\{user\}/,
	require("os").userInfo().username
), "https://jacorb90.me", console.log);