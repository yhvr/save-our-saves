// Save Our Saves v0.2
// Made with <3 by Yhvr

const { PATHS, getSaves } = require("./saver.js");

window.addEventListener("DOMContentLoaded", () => {
	const app = new window.Vue({
		el: "#app",
		data: {
			browser: "none",
			url: "https://jacorb90.github.io",
			out: {},
			save() {
				if (this.browser === "none") alert("Select a browser!");
				else {
					const [p0, p1] = this.browser.split(",");
					getSaves(
						PATHS[p0][p1].replace(
							/\{user\}/gimu,
							require("os").userInfo().username
						),
						this.url,
						out => {
							this.out = out;
						}
					);
				}
			},
			copyToClipboard(text) {
				navigator.clipboard.writeText(text).then(
					() => alert("Copied!"),
					err => {
						alert("Check the console, something happened :(");
						console.log(`THIS IS WHAT I NEED`, err)
					}
				);
			},
		},
	});
	console.log("o/");
});
